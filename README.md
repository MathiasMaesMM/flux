# Flux

## Install

**Dev**
```
flux bootstrap gitlab \
         --owner=MathiasMaesMM \
         --repository=flux \
         --branch=master \
         --token-auth \
         --path "clusters/dev" \
         --personal
```

**Prod**
```
flux bootstrap gitlab \
         --owner=MathiasMaesMM \
         --repository=flux \
         --branch=master \
         --token-auth \
         --path "clusters/prod" \
         --personal
```
